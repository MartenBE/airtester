
# AirTester

## Install

```
# apt install update
# apt install upgrade
# timedatectl set-timezone Europe/Brussels
# apt install libgpiod2 python3-libgpiod gpiod
# apt install python3-smbus python3-dev i2c-tools
# i2cdetect -y 0
# pip3 install adafruit-blinka adafruit-circuitpython-scd30
# systemctl link "$PWD/airtester.service"
# systemctl daemon-reload
# systemctl enable --now airtester.service
```

## View results

```
# tail --follow data.log
```