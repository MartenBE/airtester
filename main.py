#!/usr/bin/env python3

# https://docs.circuitpython.org/projects/scd30/en/latest/api.html

import adafruit_scd30
import adafruit_tsl2591
import argparse
import board
import busio
import logging
import prometheus_client as prom
import time
import odroid_wiringpi as wiringpi

# Configure LCD
LCD_ROW = 2 # 16 Char
LCD_COL = 16 # 2 Line
LCD_BUS = 4 # Interface 4 Bit mode

PORT_LCD_RS = 7 # GPIOY.BIT3(#83)
PORT_LCD_E = 0 # GPIOY.BIT8(#88)
PORT_LCD_D4 = 2 # GPIOX.BIT19(#116)
PORT_LCD_D5 = 3 # GPIOX.BIT18(#115)
PORT_LCD_D6 = 1 # GPIOY.BIT7(#87)
PORT_LCD_D7 = 4 # GPIOX.BIT4(#104)

wiringpi.wiringPiSetup()
lcdHandle = wiringpi.lcdInit(LCD_ROW, LCD_COL, LCD_BUS, PORT_LCD_RS, PORT_LCD_E, PORT_LCD_D4, PORT_LCD_D5, PORT_LCD_D6, PORT_LCD_D7, 0, 0, 0, 0);
wiringpi.lcdClear(lcdHandle)

parser = argparse.ArgumentParser(description="A script to periodically read the values of the SCD-30 sensor.")
parser.add_argument("-v", "--verbose", action='store_true')
args = parser.parse_args()

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s | %(levelname)-8s | %(message)s")

# Create file handler which logs debug messages
fh = logging.FileHandler("/root/airtester/data.log")
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
logger.addHandler(fh)

if args.verbose:
    # Create console handler which logs debug messages
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

# SCD-30 has tempremental I2C with clock stretching, datasheet recommends
# Starting at 50KHz
i2c = busio.I2C(board.SCL, board.SDA,frequency=50000)
scd = adafruit_scd30.SCD30(i2c)
tsl = adafruit_tsl2591.TSL2591(i2c)

scd.temperature_offset = 3
polling_interval = 5
prometheus_port = 8000

logger.info(f"Starting measurements (checking every {polling_interval} s)")
logger.info(f"Altitude: {scd.altitude} m above sea level")
logger.info(f"Ambient Pressure: {scd.ambient_pressure} mbar")
logger.info(f"Forced recalibration reference: {scd.forced_recalibration_reference} PPM")
logger.info(f"Measurement interval: {scd.measurement_interval} s")
logger.info(f"Self-calibration enabled: {scd.self_calibration_enabled}")
logger.info(f"Temperature offset: {scd.temperature_offset} C")

prom.start_http_server(prometheus_port)
gauge_co2 = prom.Gauge("co2", "CO2 expressed in PPM")
gauge_temperature = prom.Gauge("temperature", "Temperature expressed in C")
gauge_humidity = prom.Gauge("humidity", "Humidity expressed in %RH")
gauge_light = prom.Gauge("light", "Light expressed in LX")
logger.info(f"Started Prometheus HTTP endpoint at port {prometheus_port}")

while True:
    # since the measurement interval is long (2+ seconds) we check for new data before reading
    # the values, to ensure current readings.
    if scd.data_available:
        co2 = scd.CO2
        temperature = scd.temperature
        humidity = scd.relative_humidity
        lux = tsl.lux

        logger.info(f"Temperature: {temperature:6.3f} C | CO2: {co2:8.3f} PPM | Humidity: {humidity:7.3f} %RH | Light: {lux:9.3f} LX")

        gauge_co2.set(co2)
        gauge_temperature.set(temperature)
        gauge_humidity.set(humidity)
        gauge_light.set(lux)

        wiringpi.lcdClear(lcdHandle)
        lcdRow = 0
        lcdCol = 0
        wiringpi.lcdPosition(lcdHandle, lcdCol, lcdRow)
        wiringpi.lcdPrintf(lcdHandle, f"T:   {temperature:6.1f} C")
        wiringpi.lcdPosition(lcdHandle, lcdCol, (lcdRow+1))
        wiringpi.lcdPrintf(lcdHandle, f"CO2: {co2:6.1f} PPM")

    time.sleep(5)

